module ReviewConfig exposing (config)

{-| Do not rename the ReviewConfig module or the config function, because
`elm-review` will look for these.

To add packages that contain rules, add them to this review project using

    `elm install author/packagename`

when inside the directory containing this file.

-}

--import NoUnsortedCases
--import NoUnsortedRecords
--import NoUnsortedTopLevelDeclarations

import NoExposingEverything
import NoUnused.CustomTypeConstructorArgs
import NoUnused.CustomTypeConstructors
import NoUnused.Dependencies
import NoUnused.Exports
import NoUnused.Modules
import NoUnused.Parameters
import NoUnused.Patterns
import NoUnused.Variables
import Review.Rule exposing (Rule)


config : List Rule
config =
    [ NoUnused.Parameters.rule
    , NoUnused.CustomTypeConstructors.rule []
    , NoUnused.CustomTypeConstructorArgs.rule
    , NoUnused.Patterns.rule
    , NoUnused.Modules.rule
    , NoUnused.Dependencies.rule
    , NoUnused.Exports.rule

    --, NoUnused.Variables.rule
    --, NoExposingEverything.rule
    --, NoUnsortedCases.rule
    --    (NoUnsortedCases.defaults |> NoUnsortedCases.doNotSortLiterals)
    --, NoUnsortedTopLevelDeclarations.rule
    --    (NoUnsortedTopLevelDeclarations.sortTopLevelDeclarations
    --        |> NoUnsortedTopLevelDeclarations.typesFirst
    --        |> NoUnsortedTopLevelDeclarations.portsLast
    --    )
    --, NoUnsortedRecords.rule
    --    (NoUnsortedRecords.defaults
    --        |> NoUnsortedRecords.doNotSortUnknownRecords
    --    )
    ]
