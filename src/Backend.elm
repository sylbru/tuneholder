module Backend exposing (..)

import Bridge exposing (..)
import Dict exposing (Dict)
import Email exposing (Email)
import Html
import Lamdera exposing (ClientId, SessionId)
import List.Extra
import Page.Tunebook
import Time
import Tune exposing (Tune, TuneId)
import Tunebook exposing (Tunebook, TunebookId)
import Types exposing (..)


type alias Model =
    BackendModel


app :
    { init : ( Model, Cmd BackendMsg )
    , update : BackendMsg -> Model -> ( Model, Cmd BackendMsg )
    , updateFromFrontend : SessionId -> ClientId -> Types.ToBackend -> Model -> ( Model, Cmd BackendMsg )
    , subscriptions : Model -> Sub BackendMsg
    }
app =
    Lamdera.backend
        { init = init
        , update = update
        , updateFromFrontend = updateFromFrontend
        , subscriptions = subscriptions
        }


init : ( Model, Cmd BackendMsg )
init =
    ( { tunebooks = Dict.empty
      , tunebooksByUser = Dict.empty
      , loginTokens = Dict.empty
      , sessions = Dict.empty
      , now = Time.millisToPosix 0
      }
    , Cmd.none
    )


update : BackendMsg -> Model -> ( Model, Cmd BackendMsg )
update msg model =
    case msg of
        ClientConnected sessionId clientId ->
            if Dict.member sessionId model.sessions then
                ( model, Lamdera.sendToFrontend clientId GotActiveSession )

            else
                ( model, Lamdera.sendToFrontend clientId GotNoSession )

        GotTime time ->
            ( { model | now = time }, Cmd.none )


updateFromFrontend : SessionId -> ClientId -> Types.ToBackend -> Model -> ( Model, Cmd BackendMsg )
updateFromFrontend sessionId clientId msg model =
    case msg of
        LoginRequest email ->
            let
                loginToken =
                    makeLoginToken model.now email
            in
            ( { model | loginTokens = Dict.insert loginToken email model.loginTokens }, sendLoginEmail email clientId loginToken )

        VerifyLoginToken loginToken ->
            let
                findToken token =
                    Dict.get token model.loginTokens
            in
            case findToken loginToken of
                Just email ->
                    ( { model | sessions = Dict.insert sessionId email model.sessions }, Lamdera.sendToFrontend clientId (GotLoginAccepted email) )

                Nothing ->
                    ( model, Lamdera.sendToFrontend clientId GotLoginRejected )

        CreateTunebook ->
            let
                userEmail =
                    model.sessions
                        |> Dict.toList
                        |> List.filter (\( sessionId_, email ) -> sessionId_ == sessionId)
                        |> List.head
                        |> Maybe.map Tuple.second
                        |> Maybe.withDefault "nope@email"

                ( modelWithNewTunebook, newTunebookId ) =
                    createTunebook userEmail model
            in
            ( modelWithNewTunebook, Lamdera.sendToFrontend clientId (LoadedTunebook newTunebookId Tunebook.create) )

        ListTunebooks ->
            let
                userEmail =
                    model.sessions
                        |> Dict.toList
                        |> List.filter (\( sessionId_, email ) -> sessionId_ == sessionId)
                        |> List.head
                        |> Maybe.map Tuple.second
                        |> Maybe.withDefault "nope@email"

                tunebooks : List ( TunebookId, Tunebook )
                tunebooks =
                    Dict.get userEmail model.tunebooksByUser
                        |> Maybe.withDefault []
                        |> List.filterMap
                            (\tunebookId ->
                                Dict.get tunebookId model.tunebooks
                                    |> Maybe.map (\tunebook -> ( tunebookId, tunebook ))
                            )
            in
            ( model
            , Lamdera.sendToFrontend clientId (LoadedTunebooks tunebooks)
            )

        OpenTunebook id ->
            let
                maybeTunebook =
                    findTunebook id model.tunebooks
            in
            ( model
            , case maybeTunebook of
                Just tunebook ->
                    Lamdera.sendToFrontend clientId (LoadedTunebook id tunebook)

                Nothing ->
                    Cmd.none
            )

        BackendSaveTune tunebookId maybeTuneId tune ->
            let
                updatedModel : BackendModel
                updatedModel =
                    { model
                        | tunebooks =
                            Dict.update
                                tunebookId
                                (Maybe.map <|
                                    \tunebook ->
                                        case maybeTuneId of
                                            Just tuneId ->
                                                { tunebook | tunes = Dict.update tuneId (always <| Just tune) tunebook.tunes }

                                            Nothing ->
                                                { tunebook | tunes = Dict.insert (newTuneId tunebook) tune tunebook.tunes }
                                )
                                model.tunebooks
                    }
            in
            ( updatedModel
            , let
                updatedTunebook =
                    Dict.get tunebookId updatedModel.tunebooks |> Maybe.withDefault Tunebook.create
              in
              Lamdera.sendToFrontend clientId (LoadedTunebook tunebookId updatedTunebook)
            )

        BackendDeleteTune tunebookId tuneId ->
            let
                updatedModel : BackendModel
                updatedModel =
                    { model
                        | tunebooks =
                            Dict.update
                                tunebookId
                                (Maybe.map <| \tunebook -> { tunebook | tunes = Dict.remove tuneId tunebook.tunes })
                                model.tunebooks
                    }
            in
            ( updatedModel
            , let
                updatedTunebook =
                    Dict.get tunebookId updatedModel.tunebooks |> Maybe.withDefault Tunebook.create
              in
              Lamdera.sendToFrontend clientId (LoadedTunebook tunebookId updatedTunebook)
            )

        BackendSaveTunebookSettings tunebookId newName ->
            ( { model
                | tunebooks =
                    model.tunebooks
                        |> Dict.update tunebookId
                            (Maybe.map (\tunebook -> { tunebook | name = newName }))
              }
            , Lamdera.sendToFrontend sessionId (FromBackendTunebookMsg (Page.Tunebook.GotNewTunebookName tunebookId newName))
            )

        SignOut ->
            ( { model | sessions = Dict.filter (\sessionId_ _ -> sessionId_ /= sessionId) model.sessions }, Cmd.none )

        NoOpToBackend ->
            ( model, Cmd.none )

        BackendDeleteTunebook tunebookId ->
            ( model |> deleteTunebook tunebookId
            , Lamdera.sendToFrontend clientId DeletedTunebook
            )


createTunebook : Email -> Model -> ( Model, TunebookId )
createTunebook userEmail model =
    let
        newId =
            (List.maximum (Dict.keys model.tunebooks) |> Maybe.withDefault 0) + 1

        newTunebook =
            Tunebook.create

        userTunebooks =
            Dict.get userEmail model.tunebooksByUser
                |> Maybe.withDefault []
    in
    ( { model
        | tunebooks = Dict.insert newId newTunebook model.tunebooks
        , tunebooksByUser = Dict.insert userEmail (newId :: userTunebooks) model.tunebooksByUser
      }
    , newId
    )


deleteTunebook : TunebookId -> Model -> Model
deleteTunebook tunebookId model =
    { model
        | tunebooks =
            model.tunebooks
                |> Dict.remove tunebookId
        , tunebooksByUser =
            model.tunebooksByUser
                |> Dict.map (\_ ids -> List.filter ((/=) tunebookId) ids)
    }


findTunebook : TunebookId -> Dict TunebookId Tunebook -> Maybe Tunebook
findTunebook tunebookId tunebooks =
    Dict.get tunebookId tunebooks


newTuneId : Tunebook -> TuneId
newTuneId { tunes } =
    List.maximum (Dict.keys tunes)
        |> Maybe.map ((+) 1)
        |> Maybe.withDefault 0


sendLoginEmail : String -> ClientId -> String -> Cmd BackendMsg
sendLoginEmail email clientId token =
    --Debug.todo "build HTTP request to Sendinblue API: https://developers.sendinblue.com/docs/send-a-transactional-email"
    Lamdera.sendToFrontend clientId (GotLoginToken token)


makeLoginToken : Time.Posix -> String -> String
makeLoginToken now _ =
    "logintoken" ++ (String.fromInt <| Time.posixToMillis now)


subscriptions : Model -> Sub BackendMsg
subscriptions model =
    Sub.batch
        [ Lamdera.onConnect ClientConnected
        , Time.every (60 * 1000) GotTime
        ]
