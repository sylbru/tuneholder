module Bridge exposing (ToBackend(..))

import Email exposing (Email)
import Route exposing (LoginToken)
import Tune exposing (Tune, TuneId)
import Tunebook exposing (Tunebook, TunebookId)


type ToBackend
    = NoOpToBackend
    | LoginRequest String
    | CreateTunebook
    | OpenTunebook TunebookId
    | ListTunebooks
    | BackendSaveTune TunebookId (Maybe TuneId) Tune
    | BackendDeleteTune TunebookId TuneId
    | BackendSaveTunebookSettings TunebookId String
    | BackendDeleteTunebook TunebookId
    | VerifyLoginToken LoginToken
    | SignOut
