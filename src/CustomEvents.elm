module CustomEvents exposing (onEnter)

import Html.Styled as Html
import Html.Styled.Events as Events
import Json.Decode as Decode


onEnter : msg -> Html.Attribute msg
onEnter msg =
    Events.on "keypress"
        (Decode.field "code" Decode.string
            |> Decode.andThen
                (\code ->
                    if code == "Enter" then
                        Decode.succeed msg

                    else
                        Decode.fail "not the Enter key"
                )
        )
