module Evergreen.Migrate.V3 exposing (..)

import Evergreen.V1.Types as Old
import Evergreen.V3.Types as New
import Lamdera.Migrations exposing (..)


frontendModel : Old.FrontendModel -> ModelMigration New.FrontendModel New.FrontendMsg
frontendModel old =
    ModelUnchanged


backendModel : Old.BackendModel -> ModelMigration New.BackendModel New.BackendMsg
backendModel old =
    ModelUnchanged


frontendMsg : Old.FrontendMsg -> MsgMigration New.FrontendMsg New.FrontendMsg
frontendMsg old =
    MsgUnchanged


toBackend : Old.ToBackend -> MsgMigration New.ToBackend New.BackendMsg
toBackend old =
    case old of
        Old.SaveExistingTune tunebookId tuneId tune ->
            MsgMigrated ( New.BackendSaveTune tunebookId (Just tuneId) tune, Cmd.none )

        Old.SaveNewTune tunebookId tune ->
            MsgMigrated ( New.BackendSaveTune tunebookId Nothing tune, Cmd.none )

        _ ->
            MsgUnchanged


backendMsg : Old.BackendMsg -> MsgMigration New.BackendMsg New.BackendMsg
backendMsg old =
    MsgUnchanged


toFrontend : Old.ToFrontend -> MsgMigration New.ToFrontend New.FrontendMsg
toFrontend old =
    MsgUnchanged
