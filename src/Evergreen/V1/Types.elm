module Evergreen.V1.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Lamdera
import Url


type alias HomeModel =
    { tunebookId : String
    }


type alias TunebookId =
    Int


type alias TuneId =
    Int


type alias Tune =
    { name : String
    }


type alias Tunebook =
    Dict.Dict TuneId Tune


type alias InTunebookModel =
    { id : TunebookId
    , tunebook : Tunebook
    , editing : Maybe ( Maybe TuneId, Tune )
    }


type FrontendState
    = Home HomeModel
    | InTunebook InTunebookModel


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , state : FrontendState
    }


type alias BackendModel =
    Dict.Dict TunebookId Tunebook


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | BackToHome
    | TypedTunebookIdentifier String
    | RequestCreateTunebook
    | RequestOpenTunebook
    | OpenAddTune
    | TypedTuneName String
    | SaveTune
    | EditTune TuneId
    | DeleteTune TuneId
    | NoOpFrontendMsg


type ToBackend
    = NoOpToBackend
    | CreateTunebook
    | OpenTunebook TunebookId
    | SaveExistingTune TunebookId TuneId Tune
    | SaveNewTune TunebookId Tune
    | BackendDeleteTune TunebookId TuneId


type BackendMsg
    = ClientConnected Lamdera.ClientId Lamdera.SessionId


type ToFrontend
    = LoadedTunebook TunebookId Tunebook
