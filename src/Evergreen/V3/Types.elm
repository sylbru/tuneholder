module Evergreen.V3.Types exposing (..)

import Browser
import Browser.Navigation
import Dict
import Evergreen.V3.Tune
import Lamdera
import Url


type alias HomeModel =
    { tunebookId : String
    }


type alias TunebookId =
    Int


type alias TuneId =
    Int


type alias Tunebook =
    Dict.Dict TuneId Evergreen.V3.Tune.Tune


type alias InTunebookModel =
    { id : TunebookId
    , tunebook : Tunebook
    , editing : Maybe ( Maybe TuneId, Evergreen.V3.Tune.Tune )
    }


type FrontendState
    = Home HomeModel
    | InTunebook InTunebookModel


type alias FrontendModel =
    { key : Browser.Navigation.Key
    , state : FrontendState
    }


type alias BackendModel =
    Dict.Dict TunebookId Tunebook


type FrontendMsg
    = UrlClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | BackToHome
    | TypedTunebookIdentifier String
    | RequestCreateTunebook
    | RequestOpenTunebook
    | OpenAddTune
    | TypedTuneName String
    | SaveTune
    | EditTune TuneId
    | DeleteTune TuneId
    | NoOpFrontendMsg


type ToBackend
    = NoOpToBackend
    | CreateTunebook
    | OpenTunebook TunebookId
    | BackendSaveTune TunebookId (Maybe TuneId) Evergreen.V3.Tune.Tune
    | BackendDeleteTune TunebookId TuneId


type BackendMsg
    = ClientConnected Lamdera.ClientId Lamdera.SessionId


type ToFrontend
    = LoadedTunebook TunebookId Tunebook
