module Frontend exposing (..)

import Bridge
import Browser exposing (UrlRequest(..))
import Browser.Navigation as Nav
import Css
import Dict exposing (Dict)
import Html as ElmHtml
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import Json.Decode as Decode
import Lamdera
import LoginStatus exposing (LoginStatus(..))
import Page
import Page.Login
import Page.Tunebook
import Page.UserHome
import Page.VerifyLogin exposing (Model(..))
import RemoteData exposing (RemoteData(..))
import Route exposing (Route)
import Tune exposing (Tune, TuneId)
import Tunebook exposing (Tunebook, TunebookId)
import Types exposing (..)
import UI.Button
import UI.Colors
import UI.Icons
import UI.Input
import UI.Layout as Layout
import UI.Style
import Url
import Url.Parser


type alias Model =
    FrontendModel


app :
    { init : Lamdera.Url -> Nav.Key -> ( Model, Cmd FrontendMsg )
    , view : Model -> Browser.Document FrontendMsg
    , update : FrontendMsg -> Model -> ( Model, Cmd FrontendMsg )
    , updateFromBackend : ToFrontend -> Model -> ( Model, Cmd FrontendMsg )
    , subscriptions : Model -> Sub FrontendMsg
    , onUrlRequest : UrlRequest -> FrontendMsg
    , onUrlChange : Url.Url -> FrontendMsg
    }
app =
    Lamdera.frontend
        { init = init
        , onUrlRequest = UrlClicked
        , onUrlChange = UrlChanged
        , update = update
        , updateFromBackend = updateFromBackend
        , subscriptions = \m -> Sub.none
        , view = view
        }


init : Url.Url -> Nav.Key -> ( Model, Cmd FrontendMsg )
init url key =
    let
        maybeRoute =
            Route.parse url
    in
    { key = key
    , route = maybeRoute
    , page = Page.NotFound
    , loggedIn = Asking
    }
        |> loadPage maybeRoute


update : FrontendMsg -> Model -> ( Model, Cmd FrontendMsg )
update msg model =
    case msg of
        LoginMsg loginMsg ->
            case model.page of
                Page.Login loginModel ->
                    let
                        ( updatedModel, cmd ) =
                            Page.Login.update loginMsg loginModel
                    in
                    ( { model | page = Page.Login updatedModel }, Cmd.map LoginMsg cmd )

                _ ->
                    ( model, Cmd.none )

        UserHomeMsg Page.UserHome.RequestSignOut ->
            ( { model | page = Page.Login { email = "" } }, Lamdera.sendToBackend Bridge.SignOut )

        UserHomeMsg userHomeMsg ->
            case model.page of
                Page.UserHome userHomeModel ->
                    let
                        ( updatedModel, cmd ) =
                            Page.UserHome.update userHomeMsg userHomeModel
                    in
                    ( { model | page = Page.UserHome updatedModel }, Cmd.map UserHomeMsg cmd )

                _ ->
                    ( model, Cmd.none )

        TunebookMsg tunebookMsg ->
            case model.page of
                Page.Tunebook tunebookModel ->
                    let
                        ( updatedModel, cmd ) =
                            Page.Tunebook.update tunebookMsg tunebookModel
                    in
                    ( { model | page = Page.Tunebook updatedModel }, Cmd.map TunebookMsg cmd )

                _ ->
                    ( model, Cmd.none )

        UrlClicked urlRequest ->
            case urlRequest of
                Internal url ->
                    ( model
                    , Nav.pushUrl model.key (Url.toString url)
                    )

                External url ->
                    ( model
                    , Nav.load url
                    )

        UrlChanged url ->
            let
                maybeRoute : Maybe Route
                maybeRoute =
                    Route.parse url
            in
            if maybeRoute /= model.route then
                { model | route = maybeRoute } |> loadPage maybeRoute

            else
                ( model, Cmd.none )

        NoOpFrontendMsg ->
            ( model, Cmd.none )


loadPage : Maybe Route -> Model -> ( Model, Cmd FrontendMsg )
loadPage maybeRoute model =
    ( { model
        | page =
            case maybeRoute of
                Just Route.Login ->
                    Page.Login { email = "" }

                Just (Route.VerifyLoginToken _) ->
                    Page.VerifyLogin Verifying

                Just Route.UserHome ->
                    Page.UserHome Page.UserHome.init

                Just (Route.Tunebook tunebookId) ->
                    Page.Tunebook Loading

                Nothing ->
                    Page.NotFound
      }
    , case maybeRoute of
        Just Route.UserHome ->
            Lamdera.sendToBackend Bridge.ListTunebooks

        Just (Route.Tunebook tunebookId) ->
            Lamdera.sendToBackend (Bridge.OpenTunebook (String.toInt tunebookId |> Maybe.withDefault 42))

        Just (Route.VerifyLoginToken (Just loginToken)) ->
            Lamdera.sendToBackend (Bridge.VerifyLoginToken loginToken)

        _ ->
            Cmd.none
    )


updateFromBackend : ToFrontend -> Model -> ( Model, Cmd FrontendMsg )
updateFromBackend msg model =
    case msg of
        LoadedTunebook id tunebook ->
            let
                newRoute =
                    Route.Tunebook (String.fromInt id)
            in
            ( { model
                | page = Page.Tunebook <| Page.Tunebook.init id tunebook
                , route = Just newRoute
              }
            , if model.route /= Just newRoute then
                Nav.pushUrl model.key (newRoute |> Route.toUrl)

              else
                Cmd.none
            )

        LoadedTunebooks tunebooks ->
            ( { model | page = Page.UserHome (Page.UserHome.initWithTunebooks tunebooks) }, Cmd.none )

        GotLoginToken token ->
            redirectTo ("/login?token=" ++ token) model

        GotLoginAccepted email ->
            case model.page of
                Page.VerifyLogin status ->
                    ( { model | page = Page.VerifyLogin (Page.VerifyLogin.Accepted email) }, Cmd.none )

                _ ->
                    noop model

        GotLoginRejected ->
            case model.page of
                Page.VerifyLogin status ->
                    ( { model | page = Page.VerifyLogin Page.VerifyLogin.Rejected }, Cmd.none )

                _ ->
                    noop model

        GotActiveSession ->
            ( { model | loggedIn = LoggedIn }, Cmd.none )

        GotNoSession ->
            ( { model | loggedIn = NotLoggedIn }, Cmd.none )

        FromBackendTunebookMsg tunebookMsg ->
            case model.page of
                Page.Tunebook tunebookModel ->
                    let
                        ( updatedModel, cmd ) =
                            Page.Tunebook.update tunebookMsg tunebookModel
                    in
                    ( { model | page = Page.Tunebook updatedModel }, Cmd.map TunebookMsg cmd )

                _ ->
                    ( model, Cmd.none )

        DeletedTunebook ->
            model |> redirectTo "/tunebooks"


redirectTo : String -> Model -> ( Model, Cmd FrontendMsg )
redirectTo url model =
    ( model
    , Nav.pushUrl model.key url
    )


noop : model -> ( model, Cmd msg )
noop model =
    ( model, Cmd.none )


view : Model -> Browser.Document FrontendMsg
view model =
    { title = "Tuneholder"
    , body =
        -- CSS reset (based on jgthms.com/minireset.css)
        [ ElmHtml.node "style" [] [ ElmHtml.text "html,body,p,ol,ul,li,dl,dt,dd,blockquote,figure,fieldset,legend,textarea,pre,iframe,hr,h1,h2,h3,h4,h5,h6{margin:0;padding:0}h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal}ul{list-style:none}button,input,select{margin:0;font-size:inherit}html{box-sizing:border-box}*,*::before,*::after{box-sizing:inherit}img,video{height:auto;max-width:100%}iframe{border:0}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}" ]
        , ElmHtml.node "style" [] [ ElmHtml.text """
        html, body {
            height: 100%;
            color: #222;
            font-family: sans-serif;
            font-size: 100%;
        }""" ]
        , Html.toUnstyled Layout.allLayoutsStyle
        , Html.toUnstyled <|
            Html.div
                [ css
                    [ Css.height (Css.pct 100)
                    , Css.backgroundColor UI.Colors.veryLightGreen
                    ]
                ]
                [ Html.div
                    [ css
                        [ Css.height (Css.pct 100)
                        , Css.maxWidth (Css.em 60)
                        , Css.margin Css.auto
                        , Css.backgroundColor UI.Colors.lightGreen
                        , Css.boxShadow4 Css.zero Css.zero (Css.em 0.5) UI.Colors.lightGreen
                        ]
                    ]
                    [ case model.page of
                        Page.Login loginModel ->
                            Html.map LoginMsg (Page.Login.view loginModel)

                        Page.VerifyLogin status ->
                            Page.VerifyLogin.view status

                        Page.UserHome userHomeModel ->
                            Html.map UserHomeMsg (Page.UserHome.view model.loggedIn userHomeModel)

                        Page.Tunebook tunebookModel ->
                            Html.map TunebookMsg (Page.Tunebook.view model.loggedIn tunebookModel)

                        Page.NotFound ->
                            viewNotFound
                    ]
                ]
        ]
    }


viewNotFound : Html.Html FrontendMsg
viewNotFound =
    Html.div
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.justifyContent Css.center
            , Css.textAlign Css.center
            , Css.height (Css.pct 100)
            , Css.padding (Css.em 2)
            ]
        ]
        [ Html.text "This page doesn’t exist. :/"
        ]
