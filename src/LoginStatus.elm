module LoginStatus exposing (LoginStatus(..))


type LoginStatus
    = Asking
    | LoggedIn
    | NotLoggedIn
