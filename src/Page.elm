module Page exposing (Page(..))

import Page.Login
import Page.Tunebook
import Page.UserHome
import Page.VerifyLogin


type Page
    = Login Page.Login.Model
    | VerifyLogin Page.VerifyLogin.Model
    | UserHome Page.UserHome.Model
    | Tunebook Page.Tunebook.Model
    | NotFound
