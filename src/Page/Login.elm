module Page.Login exposing (Model, Msg, update, view)

import Bridge
import Css
import CustomEvents exposing (onEnter)
import Dict exposing (Dict)
import Html as ElmHtml exposing (Html)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import Lamdera
import UI.Button
import UI.Input


type alias Model =
    { email : String }


type Msg
    = TypedEmailAddress String
    | ClickedLogin


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TypedEmailAddress value ->
            ( { model | email = value }, Cmd.none )

        ClickedLogin ->
            ( model, Lamdera.sendToBackend (Bridge.LoginRequest model.email) )


view : Model -> Html Msg
view { email } =
    Html.div
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.justifyContent Css.center
            , Css.height (Css.pct 100)
            , Css.padding (Css.em 2)
            ]
        ]
        [ UI.Input.text { onInputMsg = TypedEmailAddress, value = email }
            [ Attr.type_ "email"
            , Attr.name "email"
            , Attr.placeholder "your.email@address"
            , css [ Css.textAlign Css.center ]
            , onEnter ClickedLogin
            ]
        , UI.Button.button [] { label = "Login / Sign up", onClickMsg = ClickedLogin }
        ]
