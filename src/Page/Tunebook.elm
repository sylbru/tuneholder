module Page.Tunebook exposing (Model, Msg(..), init, update, view)

import Accessibility.Aria as Aria
import Bridge
import Css
import CustomEvents exposing (onEnter)
import Dict
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import Lamdera
import LoginStatus exposing (LoginStatus(..))
import RemoteData exposing (RemoteData(..))
import Tune exposing (Tune, TuneId)
import Tunebook exposing (Tunebook, TunebookId)
import UI.Button
import UI.Checkbox
import UI.Colors
import UI.Icons
import UI.Input
import UI.Layout exposing (PropertyValue(..))
import UI.Style


type alias Model =
    RemoteData LoadedModel String


type alias LoadedModel =
    { id : TunebookId
    , tunebook : Tunebook
    , subPage : SubPage
    , tunebookNameInput : String
    , autoCapitalize : Bool
    }


type SubPage
    = ViewTunebook
    | EditTunebookSettings
    | EditTune ( Maybe TuneId, Tune )


type Msg
    = BackToTunebook
    | ClickedOpenAddTune
    | TypedTuneName String
    | TypedTuneKey String
    | SaveTune
    | ShowTune TuneId
    | DeleteTune TuneId
    | ClickedEditTunebook
    | ClickedSaveSettings
    | GotNewTunebookName TunebookId String
    | TypedTunebookName String
    | ClickedDeleteTunebook
    | CheckedCapitalize Bool


init : TunebookId -> Tunebook -> Model
init tunebookId tunebook =
    Loaded
        { id = tunebookId
        , tunebook = tunebook
        , subPage = ViewTunebook
        , tunebookNameInput = tunebook.name
        , autoCapitalize = True
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Loaded loadedModel ->
            updateLoaded msg loadedModel |> Tuple.mapFirst Loaded

        _ ->
            ( model, Cmd.none )


updateLoaded : Msg -> LoadedModel -> ( LoadedModel, Cmd Msg )
updateLoaded msg model =
    case msg of
        BackToTunebook ->
            ( { model | subPage = ViewTunebook }, Cmd.none )

        ClickedOpenAddTune ->
            ( { model | subPage = EditTune ( Nothing, Tune.create ) }, Cmd.none )

        TypedTuneName value ->
            ( case model.subPage of
                EditTune ( id, tune ) ->
                    { model | subPage = EditTune ( id, { tune | name = value } ) }

                _ ->
                    model
            , Cmd.none
            )

        SaveTune ->
            case model.subPage of
                EditTune ( Just tuneId, existingTune ) ->
                    let
                        tune =
                            Tune.normalize existingTune model.autoCapitalize
                    in
                    if Tune.isValid existingTune then
                        ( model, Lamdera.sendToBackend (Bridge.BackendSaveTune model.id (Just tuneId) tune) )

                    else
                        ( model, Cmd.none )

                EditTune ( Nothing, newTune ) ->
                    let
                        tune =
                            Tune.normalize newTune model.autoCapitalize
                    in
                    if Tune.isValid newTune then
                        ( model, Lamdera.sendToBackend (Bridge.BackendSaveTune model.id Nothing tune) )

                    else
                        ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ShowTune tuneId ->
            ( { model | subPage = EditTune ( Just tuneId, Dict.get tuneId model.tunebook.tunes |> Maybe.withDefault Tune.create ) }
            , Cmd.none
            )

        DeleteTune tuneId ->
            ( model, Lamdera.sendToBackend (Bridge.BackendDeleteTune model.id tuneId) )

        ClickedEditTunebook ->
            ( { model | subPage = EditTunebookSettings }, Cmd.none )

        ClickedSaveSettings ->
            ( model, Lamdera.sendToBackend (Bridge.BackendSaveTunebookSettings model.id model.tunebookNameInput) )

        TypedTunebookName newName ->
            ( { model | tunebookNameInput = newName }, Cmd.none )

        GotNewTunebookName tunebookId newName ->
            ( { model
                | tunebook = model.tunebook |> Tunebook.withName newName
                , subPage = ViewTunebook
              }
            , Cmd.none
            )

        ClickedDeleteTunebook ->
            ( model, Lamdera.sendToBackend (Bridge.BackendDeleteTunebook model.id) )

        CheckedCapitalize checked ->
            ( { model | autoCapitalize = checked }, Cmd.none )

        TypedTuneKey value ->
            ( case model.subPage of
                EditTune ( id, tune ) ->
                    { model | subPage = EditTune ( id, { tune | key = value } ) }

                _ ->
                    model
            , Cmd.none
            )


view : LoginStatus -> Model -> Html Msg
view loginStatus model =
    case loginStatus of
        LoggedIn ->
            case model of
                Loaded loadedModel ->
                    viewLoaded loadedModel

                _ ->
                    Html.text ""

        NotLoggedIn ->
            Html.text "Please login."

        Asking ->
            Html.text "…"


viewLoaded : LoadedModel -> Html Msg
viewLoaded model =
    Html.div
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.height (Css.pct 100)
            ]
        ]
        [ Html.nav
            [ css [ Css.backgroundColor UI.Colors.darkGreen, Css.displayFlex, Css.justifyContent Css.spaceBetween ] ]
            [ case model.subPage of
                ViewTunebook ->
                    Html.a
                        [ css [ UI.Style.buttonReset, Css.display Css.block, Css.padding2 (Css.em 0.7) (Css.em 1) ]
                        , Attr.href "/tunebooks"
                        , Aria.label "Close" |> Attr.fromUnstyled
                        ]
                        [ UI.Icons.leftArrow ]

                _ ->
                    Html.button
                        [ css [ UI.Style.buttonReset, Css.padding2 (Css.em 0.7) (Css.em 1) ]
                        , Events.onClick BackToTunebook
                        , Aria.label "Back" |> Attr.fromUnstyled
                        ]
                        [ UI.Icons.leftArrow ]
            , if model.subPage == ViewTunebook then
                UI.Button.nav { onClickMsg = ClickedEditTunebook, label = "Settings" }

              else
                Html.text ""
            ]
        , Html.main_
            [ css [ Css.displayFlex, Css.flexDirection Css.column, Css.height (Css.pct 100) ] ]
            (case model.subPage of
                ViewTunebook ->
                    tunebookView model

                EditTunebookSettings ->
                    editTunebookView model

                EditTune ( maybeTuneId, tune ) ->
                    editTuneView model.autoCapitalize maybeTuneId tune
            )
        ]


tunebookView : LoadedModel -> List (Html Msg)
tunebookView model =
    [ Html.h2
        [ css
            [ Css.margin (Css.em 1)
            , Css.textAlign Css.right
            , Css.fontWeight Css.bold
            ]
        ]
        [ Html.text <| model.tunebook.name ]
    , if not <| Dict.isEmpty model.tunebook.tunes then
        Html.ul [ css [ Css.flexGrow (Css.int 1) ] ]
            (model.tunebook.tunes
                |> Dict.toList
                |> List.map tuneView
            )

      else
        Html.p
            [ css
                [ Css.padding (Css.em 0.7)
                , Css.fontStyle Css.italic
                , Css.fontSize (Css.em 0.9)
                , Css.flexGrow (Css.int 1)
                , Css.displayFlex
                , Css.alignItems Css.center
                , Css.justifyContent Css.center
                ]
            ]
            [ Html.text "No tunes yet. Add a tune!" ]
    , UI.Button.button [ css [ Css.width (Css.pct 100) ] ] { onClickMsg = ClickedOpenAddTune, label = "Add a tune" }
    ]


editTunebookView : LoadedModel -> List (Html Msg)
editTunebookView model =
    [ Html.h2
        [ css
            [ Css.margin (Css.em 1)
            , Css.textAlign Css.right
            , Css.fontWeight Css.bold
            ]
        ]
        [ Html.text "Tunebook settings" ]
    , Html.form
        [ css
            [ Css.flexGrow (Css.int 1)
            , Css.padding (Css.em 1)
            ]
        , Attr.name "settings_form"
        , Events.onSubmit ClickedSaveSettings
        ]
        [ Html.p [ css [ Css.margin4 (Css.em 1.5) Css.zero (Css.em 0.5) Css.zero ] ]
            [ Html.label [ css [ Css.fontStyle Css.italic ] ] [ Html.text "Tunebook name" ]
            ]
        , UI.Input.text { onInputMsg = TypedTunebookName, value = model.tunebookNameInput } [ css [ Css.width (Css.pct 100) ] ]
        , Html.p [ css [ Css.margin4 (Css.em 1.5) Css.zero (Css.em 0.5) Css.zero ] ]
            [ Html.span [ css [ Css.fontStyle Css.italic, Css.color UI.Colors.danger ] ]
                [ Html.text "Danger zone" ]
            ]
        , UI.Button.danger [] { onClickMsg = ClickedDeleteTunebook, label = "Permanently delete this tunebook" }
        ]
    , UI.Button.submit [ css [ Css.width (Css.pct 100) ] ] { targetForm = Just "settings_form", label = "Save" }
    ]


tuneView : ( TuneId, Tune ) -> Html Msg
tuneView ( tuneId, tune ) =
    Html.li
        []
        [ Html.button
            [ css
                [ Css.displayFlex
                , Css.flexDirection Css.row
                , Css.alignItems Css.center
                , Css.width (Css.pct 100)
                , UI.Style.buttonReset
                , Css.hover
                    [ Css.backgroundColor UI.Colors.lighterGreen
                    ]
                ]
            , Events.onClick (ShowTune tuneId)
            ]
            [ Html.div
                [ css
                    [ Css.flexGrow (Css.int 1)
                    , Css.textAlign Css.left
                    , Css.padding2 (Css.em 0.5) (Css.em 0.75)
                    ]
                ]
                [ UI.Layout.cluster { space = Custom "1em 2em" }
                    [ Html.strong [] [ Html.text tune.name ]
                    , Html.span [] [ Html.text <| "Key: " ++ tune.key ]
                    ]
                ]
            , UI.Icons.rightArrow
            ]
        ]


editTuneView : Bool -> Maybe TuneId -> Tune -> List (Html Msg)
editTuneView autoCapitalize maybeTuneId tune =
    [ Html.div
        [ css [ Css.padding (Css.em 1), Css.flexGrow (Css.int 1) ] ]
        [ UI.Layout.stack { space = Default }
            [ Html.h2
                [ css [ Css.paddingBottom (Css.em 0.5), Css.fontWeight Css.bold, Css.fontSize (Css.em 1.2) ] ]
                [ Html.text (Tune.normalize tune autoCapitalize).name ]
            , Html.div []
                [ UI.Layout.stack { space = Custom ".5em" }
                    [ Html.label [ Attr.for "tune_name" ]
                        [ Html.text "Tune name" ]
                    , UI.Input.text
                        { onInputMsg = TypedTuneName, value = tune.name }
                        [ css [ Css.width (Css.pct 100) ]
                        , onEnter SaveTune
                        , Attr.id "tune_name"
                        ]
                    , Html.label []
                        [ UI.Checkbox.checkbox autoCapitalize CheckedCapitalize "Automatically capitalize" [] ]
                    ]
                , UI.Layout.stack { space = Custom ".5em" }
                    [ Html.label [ Attr.for "tune_key" ] [ Html.text "Key" ]
                    , UI.Input.text
                        { onInputMsg = TypedTuneKey, value = tune.key }
                        [ css [ Css.width (Css.pct 100) ]
                        , onEnter SaveTune
                        , Attr.id "tune_key"
                        ]
                    ]
                ]
            ]
        ]
    , Html.div
        [ css [ Css.displayFlex ] ]
        (List.filterMap identity <|
            [ Just <| UI.Button.button [ css [ Css.flexGrow (Css.int 1) ] ] { onClickMsg = SaveTune, label = "Save" }
            , maybeTuneId |> Maybe.map (\id -> UI.Button.button [ css [ Css.flexGrow (Css.int 1) ] ] { onClickMsg = DeleteTune id, label = "Delete" })
            ]
        )
    ]
