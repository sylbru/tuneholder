module Page.UserHome exposing (Model, Msg(..), init, initWithTunebooks, update, view)

import Bridge
import Css
import CustomEvents exposing (onEnter)
import Html as ElmHtml exposing (Html)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import Lamdera
import LoginStatus exposing (LoginStatus(..))
import Route
import Tune exposing (TuneId)
import Tunebook exposing (Tunebook, TunebookId)
import UI.Button
import UI.Input


type alias Model =
    { tunebooks : List ( TunebookId, Tunebook )
    }


type Msg
    = RequestCreateTunebook
    | RequestOpenTunebook TunebookId
    | RequestSignOut


init : Model
init =
    { tunebooks = [] }


initWithTunebooks : List ( TunebookId, Tunebook ) -> Model
initWithTunebooks tunebooks =
    { tunebooks = tunebooks }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        RequestCreateTunebook ->
            ( model, Lamdera.sendToBackend Bridge.CreateTunebook )

        RequestOpenTunebook tunebookId ->
            ( model, Lamdera.sendToBackend (Bridge.OpenTunebook tunebookId) )

        RequestSignOut ->
            -- Handled in Frontend update
            ( model, Cmd.none )


view : LoginStatus -> Model -> Html Msg
view loginStatus model =
    Html.div
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.justifyContent Css.spaceAround
            , Css.height (Css.pct 100)
            , Css.padding (Css.em 2)
            ]
        ]
        (case loginStatus of
            LoggedIn ->
                [ Html.h2 [ css [ Css.fontWeight Css.bold ]] [ Html.text "My tunebooks" ]
                , Html.section [ css [ Css.displayFlex, Css.property "gap" "1em" ] ]
                    (List.map
                        (\( tunebookId, tunebook ) ->
                            UI.Button.link []
                                { label = tunebook.name, url = "/tunebook/" ++ String.fromInt tunebookId }
                        )
                        model.tunebooks
                    )
                , UI.Button.button [] { label = "Create a tunebook", onClickMsg = RequestCreateTunebook }
                , UI.Button.button [] { label = "Sign out", onClickMsg = RequestSignOut }
                ]

            NotLoggedIn ->
                [ Html.p [ css [ Css.textAlign Css.center ] ]
                    [ Html.text "Please "
                    , Html.a [ Attr.href (Route.toUrl Route.Login) ] [ Html.text "login" ]
                    , Html.text "."
                    ]
                ]

            Asking ->
                [ Html.p [ css [ Css.textAlign Css.center ] ]
                    [ Html.text "…"
                    ]
                ]
        )
