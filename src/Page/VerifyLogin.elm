module Page.VerifyLogin exposing (Model(..), Msg, update, view)

import Bridge
import Css
import CustomEvents exposing (onEnter)
import Dict exposing (Dict)
import Email exposing (Email)
import Html as ElmHtml exposing (Html)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import Lamdera
import UI.Button
import UI.Input


type Model
    = Verifying
    | Accepted Email
    | Rejected


type Msg
    = NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model, Cmd.none )


view : Model -> Html msg
view status =
    Html.div
        [ css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.justifyContent Css.center
            , Css.height (Css.pct 100)
            , Css.padding (Css.em 2)
            ]
        ]
        [ Html.p [ css [ Css.textAlign Css.center ] ]
            [ Html.text <|
                case status of
                    Verifying ->
                        "Verifying login token…"

                    Accepted email ->
                        "Logged in as " ++ email ++ "!"

                    Rejected ->
                        "Login token is invalid."
            ]
        ]
