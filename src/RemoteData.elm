module RemoteData exposing (RemoteData(..), map)


type RemoteData data err
    = Loaded data
    | Errored err
    | Loading
    | NotAsked


map : (data -> data) -> RemoteData data err -> RemoteData data err
map fn remoteData =
    case remoteData of
        Loaded data ->
            Loaded (fn data)

        _ ->
            remoteData
