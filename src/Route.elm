module Route exposing (LoginToken, Route(..), parse, toUrl)

import Url exposing (Url)
import Url.Builder as Builder
import Url.Parser exposing ((</>), (<?>), Parser, map, oneOf, s, string, top)
import Url.Parser.Query as Query


type alias LoginToken =
    String


type Route
    = Login
    | VerifyLoginToken (Maybe LoginToken)
    | UserHome
    | Tunebook String


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map Login <|
            top
        , map VerifyLoginToken
            (s "login" <?> Query.string "token")
        , map UserHome <|
            s "tunebooks"
        , map Tunebook <|
            (s "tunebook" </> string)
        ]


parse : Url -> Maybe Route
parse url =
    Url.Parser.parse routeParser url


toUrl : Route -> String
toUrl route =
    case route of
        Login ->
            Builder.absolute [] []

        VerifyLoginToken loginToken ->
            Builder.absolute [ "login" ] [ Builder.string "token" (loginToken |> Maybe.withDefault "") ]

        UserHome ->
            Builder.absolute [ "tunebooks" ] []

        Tunebook id ->
            Builder.absolute [ "tunebook", id ] []
