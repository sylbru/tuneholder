module TitleCase exposing (titleCase)


titleCase : String -> String
titleCase name =
    let
        capitalizeWord : String -> Bool -> String
        capitalizeWord word isFirstOrLast =
            if
                isFirstOrLast
                    || ((not <| isArticle word)
                            && ((not <| isPreposition word) || String.length word >= 4)
                            && ((not <| isConjunction word) || String.length word >= 4)
                       )
            then
                capitalize word

            else
                String.toLower word
    in
    name
        |> String.words
        |> (\words ->
                List.indexedMap
                    (\i word ->
                        capitalizeWord word
                            (i == 0 || i == (List.length words - 1))
                    )
                    words
           )
        |> String.join " "


capitalize : String -> String
capitalize word =
    word
        |> String.toList
        |> (\list ->
                case list of
                    first :: rest ->
                        Char.toLocaleUpper first :: List.map Char.toLocaleLower rest

                    [] ->
                        []
           )
        |> String.fromList


isArticle : String -> Bool
isArticle word =
    List.member
        (String.toLower word)
        [ "the", "a", "an" ]


isConjunction : String -> Bool
isConjunction word =
    List.member
        (String.toLower word)
        [ "for", "and", "nor", "but", "or", "yet", "so", "either", "neither" ]



-- [ "after", "although", "as", "as if", "because", "before", "even", "if", "lest ", "once", "only", "since", "so", "supposing", "that", "than", "though", "till", "unless", "until", "when", "whenever", "where", "whereas", "wherever", "while "]


isPreposition : String -> Bool
isPreposition word =
    List.member
        (String.toLower word)
        [ "about", "above", "across", "after", "against", "along", "amid", "among", "anti", "around", "as", "at", "before", "behind", "below", "beneath", "beside", "besides", "between", "beyond", "but", "by", "concerning", "considering", "despite", "down", "during", "except", "excepting", "excluding", "following", "for", "from", "in", "inside", "into", "like", "minus", "near", "of", "off", "on", "onto", "opposite", "outside", "over", "past", "per", "plus", "regarding", "round", "save", "since", "than", "through", "to", "toward", "towards", "under", "underneath", "unlike", "until", "up", "upon", "versus", "via", "with", "within", "without" ]
