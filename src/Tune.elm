module Tune exposing (Tune, TuneId, create, isValid, normalize)

import TitleCase


type alias TuneId =
    Int


type alias Tune =
    { name : String
    , key : String
    }


create : Tune
create =
    { name = "", key = "G" }


isValid : Tune -> Bool
isValid tune =
    not <| String.isEmpty tune.name


normalize : Tune -> Bool -> Tune
normalize tune autoCapitalize =
    if autoCapitalize then
        { tune | name = TitleCase.titleCase tune.name }

    else
        { tune | name = String.trim tune.name }
