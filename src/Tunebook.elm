module Tunebook exposing (Tunebook, TunebookId, create, withName)

import Dict exposing (Dict)
import Tune exposing (Tune, TuneId)


type alias TunebookId =
    Int


type alias Tunebook =
    { name : String, tunes : Dict TuneId Tune }


create : Tunebook
create =
    { name = "New tunebook", tunes = Dict.empty }


withName : String -> Tunebook -> Tunebook
withName newName tunebook =
    { tunebook | name = newName }
