module Types exposing (..)

import Bridge
import Browser exposing (UrlRequest)
import Browser.Navigation exposing (Key)
import Dict exposing (Dict)
import Email exposing (Email)
import Lamdera exposing (ClientId, SessionId)
import LoginStatus exposing (LoginStatus(..))
import Page exposing (Page)
import Page.Login
import Page.Tunebook
import Page.UserHome
import Route exposing (LoginToken, Route)
import Time
import Tune exposing (Tune, TuneId)
import Tunebook exposing (Tunebook, TunebookId)
import Url exposing (Url)


type alias FrontendModel =
    { key : Key
    , route : Maybe Route
    , page : Page
    , loggedIn : LoginStatus
    }


type alias BackendModel =
    { tunebooks : Dict TunebookId Tunebook
    , tunebooksByUser : Dict Email (List TunebookId)
    , loginTokens : Dict LoginToken Email
    , sessions : Dict SessionId Email
    , now : Time.Posix
    }


type FrontendMsg
    = UrlClicked UrlRequest
    | UrlChanged Url
    | LoginMsg Page.Login.Msg
    | UserHomeMsg Page.UserHome.Msg
    | TunebookMsg Page.Tunebook.Msg
    | NoOpFrontendMsg


type alias ToBackend =
    Bridge.ToBackend


type BackendMsg
    = ClientConnected ClientId SessionId
    | GotTime Time.Posix


type ToFrontend
    = LoadedTunebook TunebookId Tunebook
    | LoadedTunebooks (List ( TunebookId, Tunebook ))
    | GotLoginToken String
    | GotLoginAccepted Email
    | GotLoginRejected
    | GotActiveSession
    | GotNoSession
    | FromBackendTunebookMsg Page.Tunebook.Msg
    | DeletedTunebook
