module UI.Button exposing (..)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import UI.Colors
import UI.Style


commonStyle : Css.Style
commonStyle =
    Css.batch
        [ Css.borderWidth Css.zero
        , Css.padding2 (Css.em 0.6) (Css.em 0.8)
        , Css.backgroundColor UI.Colors.darkGreen
        , Css.color UI.Colors.white
        , Css.boxShadow4 Css.zero (Css.em 0.1) (Css.em 0.25) (Css.hex "#aaaa")
        , Css.borderRadius (Css.em 0.1)
        , Css.textDecoration Css.none
        , Css.hover
            [ Css.backgroundColor UI.Colors.mediumDarkGreen
            , Css.cursor Css.pointer
            ]
        ]


link : List (Html.Attribute msg) -> { label : String, url : String } -> Html msg
link attrs { label, url } =
    Html.a
        ([ Attr.href url
         , css [ commonStyle ]
         ]
            ++ attrs
        )
        [ Html.text label ]


button : List (Html.Attribute msg) -> { label : String, onClickMsg : msg } -> Html msg
button attrs { label, onClickMsg } =
    Html.button
        (attrs
            ++ [ Events.onClick onClickMsg
               , css [ commonStyle ]
               , Attr.type_ "button"
               ]
        )
        [ Html.text label ]


danger : List (Html.Attribute msg) -> { label : String, onClickMsg : msg } -> Html msg
danger attrs { label, onClickMsg } =
    Html.button
        (attrs
            ++ [ Events.onClick onClickMsg
               , css [ commonStyle, Css.backgroundColor UI.Colors.danger, Css.hover [ Css.backgroundColor UI.Colors.danger ] ]
               , Attr.type_ "button"
               ]
        )
        [ Html.text label ]


submit : List (Html.Attribute msg) -> { label : String, targetForm : Maybe String } -> Html msg
submit attrs { label } =
    Html.button
        ([ css [ commonStyle ]
         , Attr.type_ "submit"
         ]
            ++ attrs
        )
        [ Html.text label ]


nav : { onClickMsg : msg, label : String } -> Html msg
nav { onClickMsg, label } =
    Html.button
        [ Events.onClick onClickMsg
        , css
            [ UI.Style.buttonReset
            , Css.color UI.Colors.white
            , Css.padding2 Css.zero (Css.em 0.5)
            , Css.hover [ Css.backgroundColor (Css.hex "#fff2") ]
            ]
        ]
        [ Html.text label ]
