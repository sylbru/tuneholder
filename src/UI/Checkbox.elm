module UI.Checkbox exposing (checkbox)

import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Events


checkbox : Bool -> (Bool -> msg) -> String -> List (Attribute msg) -> Html msg
checkbox checked msg label attrs =
    Html.label []
        [ Html.input
            ([ Attr.type_ "checkbox"
             , Attr.checked checked
             , Events.onCheck msg
             ]
                ++ attrs
            )
            []
        , Html.text <| " " ++ label
        ]
