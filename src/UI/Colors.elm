module UI.Colors exposing (..)

import Css exposing (Color, hex, hsl, hsla, rgb, rgba)


darkGreen : Color
darkGreen =
    hex "15795B"


mediumDarkGreen : Color
mediumDarkGreen =
    hex "1A936F"


mediumGreen : Color
mediumGreen =
    hex "87BEAE"


white : Color
white =
    rgb 255 255 255


lightGreen : Color
lightGreen =
    hex "D2EFD8"


lighterGreen : Color
lighterGreen =
    hex "E1F4E5"


veryLightGreen : Color
veryLightGreen =
    hex "F0FAF2"


danger : Color
danger =
    hex "cc0000"
