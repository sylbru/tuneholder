module UI.Icons exposing (cross, leftArrow, rightArrow)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import UI.Colors


rightArrow : Html msg
rightArrow =
    Html.div
        [ css
            [ Css.width (Css.em 0.75)
            , Css.height (Css.em 0.75)
            , Css.border3 (Css.px 1) Css.solid UI.Colors.darkGreen
            , Css.borderLeftWidth Css.zero
            , Css.borderBottomWidth Css.zero
            , Css.transform (Css.rotate (Css.deg 45))
            , Css.margin2 Css.zero (Css.em 0.7)
            , Css.flexShrink Css.zero
            ]
        ]
        [ Html.text "" ]


leftArrow : Html msg
leftArrow =
    Html.div
        [ css
            [ Css.width (Css.em 1)
            , Css.height (Css.em 1)
            , Css.border3 (Css.px 1) Css.solid UI.Colors.veryLightGreen
            , Css.borderRightWidth Css.zero
            , Css.borderBottomWidth Css.zero
            , Css.transform (Css.rotate (Css.deg -45))
            ]
        ]
        []


cross : Html msg
cross =
    Html.div
        [ css
            [ Css.width (Css.em 1)
            , Css.height (Css.em 1)
            , Css.border3 (Css.px 1) Css.solid UI.Colors.veryLightGreen
            , Css.borderRightWidth Css.zero
            , Css.borderBottomWidth Css.zero
            , Css.transform (Css.rotate (Css.deg -45))
            ]
        ]
        []
