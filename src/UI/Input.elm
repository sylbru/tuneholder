module UI.Input exposing (..)

import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr exposing (css)
import Html.Styled.Events as Events
import UI.Colors


commonStyle : Css.Style
commonStyle =
    Css.batch
        [ Css.color Css.inherit
        , Css.backgroundColor UI.Colors.veryLightGreen
        , Css.border3 (Css.px 1) Css.solid UI.Colors.mediumDarkGreen
        , Css.padding2 (Css.em 0.5) (Css.em 0.7)
        ]


text : { onInputMsg : String -> msg, value : String } -> List (Html.Attribute msg) -> Html msg
text { onInputMsg, value } attrs =
    Html.input
        ([ css [ commonStyle ]
         , Attr.type_ "text"
         , Events.onInput onInputMsg
         , Attr.value value
         ]
            ++ attrs
        )
        []


number : { onInputMsg : String -> msg, value : String } -> Html msg
number { onInputMsg, value } =
    Html.input
        [ css [ commonStyle, Css.textAlign Css.center ]
        , Attr.type_ "number"
        , Events.onInput onInputMsg
        , Attr.value value
        ]
        []
