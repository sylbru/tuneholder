module UI.Layout exposing (PropertyValue(..), allLayoutsStyle, cluster, stack, variable)

import Css
import Css.Global as Global exposing (Snippet)
import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attr
import Page.Login exposing (Msg)


type PropertyValue
    = Default
    | Custom String


cluster : { space : PropertyValue } -> List (Html msg) -> Html msg
cluster config children =
    Html.div
        ([ Attr.class "cluster" ]
            ++ (case config.space of
                    Custom customSpace ->
                        [ variable "--space" customSpace ]

                    _ ->
                        []
               )
        )
        children


stack : { space : PropertyValue } -> List (Html msg) -> Html msg
stack config children =
    Html.div
        ([ Attr.class "stack" ]
            ++ (case config.space of
                    Custom customSpace ->
                        [ variable "--space" customSpace ]

                    _ ->
                        []
               )
        )
        children


variable : String -> String -> Attribute msg
variable name value =
    Attr.attribute "style" (name ++ ": " ++ value)


clusterStyles : List Snippet
clusterStyles =
    [ Global.class "cluster"
        [ Css.displayFlex
        , Css.flexWrap Css.wrap
        , Css.property "gap" "var(--space, 1rem)"
        ]
    ]


stackStyles : List Snippet
stackStyles =
    [ Global.class "stack"
        [ Global.children
            [ Global.everything
                [ Global.adjacentSiblings
                    [ Global.everything
                        [ Css.property "margin-block-start" "var(--space, 1rem)" ]
                    ]
                ]
            ]
        ]
    ]


allLayoutsStyle : Html msg
allLayoutsStyle =
    [ clusterStyles, stackStyles ]
        |> List.concat
        |> Global.global
