module UI.Style exposing (buttonReset)

import Css exposing (Style)


buttonReset : Style
buttonReset =
    Css.batch
        [ Css.backgroundColor Css.transparent
        , Css.backgroundImage Css.none
        , Css.borderWidth Css.zero
        , Css.cursor Css.pointer
        ]
