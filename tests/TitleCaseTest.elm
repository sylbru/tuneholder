module TitleCaseTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import TitleCase


suite : Test
suite =
    describe "TitleCase"
        [ test "One word" <|
            \_ ->
                "boogerman"
                    |> TitleCase.titleCase
                    |> Expect.equal "Boogerman"
        , test "Two words" <|
            \_ ->
                "muddy creek"
                    |> TitleCase.titleCase
                    |> Expect.equal "Muddy Creek"
        , test "Articles are lowercased" <|
            \_ ->
                "Speed The Plow"
                    |> TitleCase.titleCase
                    |> Expect.equal "Speed the Plow"
        , test "Prepositions/conjunctions are lowercased" <|
            \_ ->
                "Bull At The Wagon"
                    |> TitleCase.titleCase
                    |> Expect.equal "Bull at the Wagon"
        , test "First and last words are always uppercased" <|
            \_ ->
                "the Tom Biggsby river waltz"
                    |> TitleCase.titleCase
                    |> Expect.equal "The Tom Biggsby River Waltz"
        , test "First and last words are always uppercased (2)" <|
            \_ ->
                "turn your radio on"
                    |> TitleCase.titleCase
                    |> Expect.equal "Turn Your Radio On"
        , test "First and last words are always uppercased (3)" <|
            \_ ->
                "A VISIT FROM THE GOON SQUAD"
                    |> TitleCase.titleCase
                    |> Expect.equal "A Visit From the Goon Squad"
        , test "Uppercase long prepositions/conjunctions" <|
            \_ ->
                "sal's got mud between her toes"
                    |> TitleCase.titleCase
                    |> Expect.equal "Sal's Got Mud Between Her Toes"
        ]
