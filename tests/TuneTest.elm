module TuneTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Tune


suite : Test
suite =
    describe "Tune"
        [ isValid ]


isValid : Test
isValid =
    describe "isValid"
        [ test "Non-empty tune name is valid" <|
            \_ ->
                { name = "Muddy Creek" }
                    |> Tune.isValid
                    |> Expect.true "Expected non-empty tune name to be valid"
        , test "Empty tune name is invalid" <|
            \_ ->
                { name = "" }
                    |> Tune.isValid
                    |> Expect.false "Expected empty tune name to be invalid"
        ]
